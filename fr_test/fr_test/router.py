from rest_framework import routers

from notifications.router import register as notifications_register

router = routers.DefaultRouter()

notifications_register(router)
