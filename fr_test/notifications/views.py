from rest_framework import mixins
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import GenericViewSet

from .models import Employee, Mailing, Message
from .serializers import EmployeeSerializer, MailingSerializer, MessageSerializer


class EmployeeViewSet(mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.DestroyModelMixin,
                      mixins.ListModelMixin,
                      GenericViewSet):
    """
    Employee CRUD methods.
    """

    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

    permission_classes = [AllowAny]


class MailingViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     mixins.DestroyModelMixin,
                     GenericViewSet):
    """
    Mailing CRUD methods.
    """

    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    permission_classes = [AllowAny]
