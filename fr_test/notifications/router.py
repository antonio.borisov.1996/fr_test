from .views import MailingViewSet, EmployeeViewSet


def register(router):
    router.register('mailing', MailingViewSet, basename='mailing')
    router.register('employee', EmployeeViewSet, basename='employee')
