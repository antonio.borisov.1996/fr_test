from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone


class Mailing(models.Model):
    """Рассылка"""

    date_start = models.DateTimeField(default=timezone.now)
    date_finish = models.DateTimeField()
    text = models.TextField()
    operator_code = models.CharField('Код оператора', max_length=10)
    tag = models.CharField('Тэг', max_length=10)
    started = models.BooleanField(default=False)

    def __str__(self):
        return '{} {}'.format(self.id, self.text)


class Employee(models.Model):
    """
    Модель клиент
    """

    phone_number = models.IntegerField(validators=[MinValueValidator(70000000000),
                                                   MaxValueValidator(79999999999)])
    operator_code = models.CharField('Код оператора', max_length=10)
    tag = models.CharField('Тэг', max_length=10)
    timezone = models.SmallIntegerField()

    def __str__(self):
        return str(self.phone_number)


class Message(models.Model):
    """
    Модель сообщение
    """

    date_start = models.DateTimeField(default=None, null=True, blank=True)
    status = models.CharField('Статус отправки', max_length=10, default='в очереди')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='messages_mailing')
    recipient = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='recipient_recipient')

    def __str__(self):
        return str(self.id)
