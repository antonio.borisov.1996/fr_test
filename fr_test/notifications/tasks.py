from celery import shared_task
import requests
from .models import Message, Mailing, Employee
from django.utils import timezone
from django.conf import settings


@shared_task
def send_mail(user_id, user_phone, mailing_text, mailing_date_finish,
              mailing_date_start, mailing_id, message_id):
    now = timezone.now()
    message = Message.objects.get(id=message_id)
    if now >= mailing_date_start and now < mailing_date_finish:
        token = settings.INTEGRATION_TOKEN
        d = {
            "id": user_id,
            "phone": user_phone,
            "text": mailing_text
        }
        response = requests.post('https://probe.fbrq.cloud/send/{}/'.format(mailing_id), data=d,
                                 headers={'bearerAuth': 'Bearer {}'.format(token)})

        if response.status_code == 200:
            message.status = 'ok'
        else:
            message.status = 'failed'
    else:
        message.status = 'overdue'
    return response


@shared_task
def send_mails():
    now = timezone.now()
    mailings = Mailing.objects.filter(date_start__gte=now, date_finish__lt=now)
    for mailing in mailings:
        filters = {}
        if mailing.operator_code is not None:
            filters['operator_code'] = mailing.operator_code
        if mailing.tag is not None:
            filters['tag'] = mailing.tag
        recipients = Employee.objects.filter(**filters)
        for recipient in recipients:
            message = Message(recipient=recipient, mailing=mailing)
            send_mail.delay(
                recipient.id, recipient.phone, mailing.text,
                mailing.id, mailing.date_start, mailing.date_finish, message.id)
