"""
Account API serializers
"""
from django.db.models import Sum
from rest_framework import serializers

from .models import  Employee, Mailing, Message


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = ('id', 'phone_number', 'operator_code', 'tag', 'timezone', )
        read_only_fields = ('id', )


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'date_start', 'status', 'mailing', 'recipient', )
        read_only_fields = ('id', )


class MailingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mailing
        fields = ('id', 'date_start', 'date_finish', 'text', )
        read_only_fields = ('id', )
        lookup_field = 'id'
